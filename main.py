#!/usr/bin/env python3

from __future__ import print_function

import sys


def main(*args):
    return 0


if __name__ == '__main__':
    sys.exit(main(*sys.argv[1:]))
