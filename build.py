#!/usr/bin/env python3

import os
import sys
import zipfile


def main():
    source_root = os.path.dirname(__file__)
    output_path = os.path.join(source_root, "dist.zip")

    collect_files = [
        "main.py",
        "requirements.txt",
    ]

    with zipfile.ZipFile(output_path, "w", zipfile.ZIP_BZIP2, compresslevel=9) as zf:
        for f in collect_files:
            src = os.path.join(source_root, f)
            assert os.path.isfile(src)
            zf.write(src, arcname="dist/" + f)

    return 0


if __name__ == '__main__':
    sys.exit(main())
